# Hodor Library - Kotlin

Esta librería sirve como un ejemplo de proyecto Kotlin con estructura de Gradle.

Consiste en una clase `org.rafaolivas.Hodor` con un único método `speak()` que devuelve el string *"hodor"*.

---

## Compilación

Para construir el proyecto es necesario contar con lo siguiente:

- JDK 8 ó mayor.
- Gradle 5.0 ó mayor.

Después seguir los siguientes pasos:

1. En la raíz del proyecto, ejecutar el comando desde terminal `gradle build`.
2. Esto generará nuevos directorios. El archivo `.jar` puede encontrarse en la ruta `build/libs/`.